# Inhalt

Dieses Projekt enthält eine einfache Access-Datenbank (Access 2016) mit VBA-Code

* Tabelle "Adressen" mit einfacher Struktur
* Formular "Adressen", an die Tabelle gebunden (Einzelformularansicht)
* VBA-Code zum Erzeugen von Testdaten

Die leere `.accdb`-Datei ist im Repository enthalten.

# Erzeugen der Testdaten

* Öffne das Modul "Testdatengenerator"
* Öffne ggf. den Direktbereich (Strg + g)
* Rufe im Direktbereich die `Sub Testdaten` auf und gib als Parameter die Anzahl der Datensätze an

Achtung! Bei einer Million Datensätze kann das schon einen Moment dauern :-)
